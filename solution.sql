-- Insert users in users Table
INSERT INTO users(email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passowrdA", "2021-01-01 01:00:00");
INSERT INTO users(email, password, datetime_created) VALUES ("juandelacruz@gmail.com", "passowrdB", "2021-01-01 02:00:00");
INSERT INTO users(email, password, datetime_created) VALUES ("janesmith@gmail.com", "passowrdC", "2021-01-01 03:00:00");
INSERT INTO users(email, password, datetime_created) VALUES ("mariadelacruz@gmail.com", "passowrdD", "2021-01-01 04:00:00");
INSERT INTO users(email, password, datetime_created) VALUES ("johndoe@gmail.com", "passowrdE", "2021-01-01 05:00:00");

-- Insert Posts in posts Table
INSERT INTO posts(author_id, title, content, datetime_posted) VALUES (1, "First Code", "Hello World!", "2021-01-02 01:00:00");
INSERT INTO posts(author_id, title, content, datetime_posted) VALUES (1, "Second Code", "Hello Earth!", "2021-01-02 02:00:00");
INSERT INTO posts(author_id, title, content, datetime_posted) VALUES (2, "Third Code", "Welcome to Mars!", "2021-01-02 03:00:00");
INSERT INTO posts(author_id, title, content, datetime_posted) VALUES (4, "Fourth Code", "Bye bye solar system!", "2021-01-02 04:00:00");

-- Get all the post with an Author ID of 1;
SELECT * FROM posts WHERE author_id = 1;

-- Get all the user`s email and datetime of creation.
SELECT email, datetime_created FROM users;

-- Update a post Content "Hello to the people of the Earth!" where its initial content is "Hello Earth!" by using the record`s ID
Update posts SET content = "Hello to the people of the Earth!" WHERE content = "Hello Earth!"  AND id = 2;

-- DELETE the user with an email of "johndoe@gmail.com";
DELETE FROM users Where email = "johndoe@gmail.com";

